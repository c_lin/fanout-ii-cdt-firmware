/* 
    error_coder
    C. Lin, chiehlin@uchicago.edu
    
    Three levels of errors:
	         |  Priority  | pattern | types
   ----------------------------------------------------------------				
	- ERROR  | 1st	      | 100     | pending, spill no, event no
	- STOP   | 2nd        | 110     | memory almost full
	- WARNING| 3rd        | 111     | energy word error

    2022.01.12 updated
    WARNING is omitted.
	Prioritization is designed to avoid that both ERROR and STOP happen at the same time (within 3 clocks).
*/

module error_coder
#( parameter ERROR_LENGTH = 3 )
(
  //  
  clk               , 
  reset             ,

  // inputs
  err               ,
  stop_rising       ,
  stop_falling      ,
  
  // outputs
  q

);


input wire        clk;
input wire        reset;

// inputs
input wire        err;
input wire        stop_rising;
input wire        stop_falling;

// inputs
output reg        q;

//
reg               lock = 1'b0;
reg               err_sent = 1'b0;
reg        [1 :0] level = 0;
reg        [1 :0] cnt = 0;

////////////////////////////////////////////
always @(posedge clk) begin

    //
    // Reset
    // 
    if( reset == 1'b1 ) begin
        err_sent <= 1'b0;
        lock <= 1'b0;
        cnt <= 0;
    end

    //
    // Error output logic
    //
    if( cnt == ERROR_LENGTH ) begin
	    lock <= 1'b0;
	    q <= 1'b0;
	    cnt <= 0;
    end 
	else if( lock == 1'b1 ) begin
	    q <= ( cnt < level ) ? 1'b1 : 1'b0;
	    cnt <= cnt + 1;
	end
	else begin
	    q <= 1'b0;
	end

    //
    // Send error / stop if it is not locked and no error is sent in this spill.
    // 
    // 1.   "err" signal keeps 1'b1 during the entire spill.
    //      If the output is locked due to STOP, ERROR is pending.
    // 
    // 2.   ERROR has a higher priority than STOP.
    //      If the output is locked due to ERROR, the STOP signal will NOT be sent.
    //      However, it is okay because no trigger will be issued toward ERROR.
    //
    if( lock == 1'b0 && err_sent == 1'b0 && err == 1'b1 ) begin
        lock <= 1'b1;
        err_sent <= 1'b1;
        level <= 1;
        cnt <= 0;
    end
    else if( lock == 1'b0 && (stop_rising==1'b1 || stop_falling==1'b1) ) begin
        lock <= 1'b1;
        level <= 2;
        cnt <= 0;
    end

end

endmodule