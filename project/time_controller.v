/* 
   Top CDT Design
   time_controller block
   C. Lin, 2018.03.27, ver 3
   
*/

module time_controller
#( parameter LENGTH = 4  ,
   parameter DELAY_ALIGN = 12500000 ,
   parameter SPILL_LENGTH = 350000000 ,
   parameter MAX_ALIGN_TIME = 500
 )
// other variables
(
  clk               , // system clock
  
  // input
  in_live           , // live input ( 1  bit )
  in_et             , // 17-bits et ( 16-bits raw + 1-bit is_et )
  in_veto           , // 16-bits veto
  in_err            , // err bit
  
  // register
  raw_delay         , // delay to issue et&veto 
 
  // output
  out_adc           , // trigger line to adc
  out_rst           , // connect with blocks for resetting
  out_veto          , // veto output
  out_is_et         , // 1-bit et output
  out_raw_et        , // 16-bits et output (debugging)
  out_spill         , // spill-on or not (for other blocks)
  out_ena           ,
  out_delay_et      ,
  out_align_err     ,
  timestamp     
 
);

input wire         clk;

// input
input wire         in_live;
input wire [16 :0] in_et;
input wire [15 :0] in_veto;    
input wire         in_err;

// register
input wire [8  :0] raw_delay;

// output
output reg         out_adc;
output reg         out_rst;
output reg [15: 0] out_veto;
output reg         out_is_et;
output reg [15: 0] out_raw_et;
output reg         out_spill;
output reg         out_ena;
output reg [8 : 0] out_delay_et;
output reg [31 :0] timestamp;  
output reg         out_align_err;

// internal variable
reg                pre_live;
reg                spill_on;

reg        [9  :0] et_cnt;
reg        [9  :0] veto_cnt;

reg        [16 :0] et_tmp0;
reg        [16 :0] et_tmp1;
reg        [16 :0] et_tmp2;

reg        [15 :0] veto_tmp0;
reg        [15 :0] veto_tmp1;
reg        [15 :0] veto_tmp2;

reg                align_ena;
reg        [8:  0] align_cnt;
reg                rena;

reg        [8:  0] raddr_start_et;
reg        [8:  0] raddr_start_veto;

reg        [8  :0] delay_et;


// include
reg        [8  :0] raddr_et;
reg        [8  :0] raddr_veto;
reg        [8  :0] waddr_et;
reg        [8  :0] waddr_veto;

wire       [16 :0] q_wire_et;
wire       [15 :0] q_wire_veto;

et_raw_mem _etmem(clk,in_et,raddr_et,waddr_et,1'b1,q_wire_et);
raw_mem    _vetomem(clk,in_veto,raddr_veto,waddr_veto,1'b1,q_wire_veto);

always @(posedge clk)
begin
 
// init
   if( in_live == 1'b1 )
      timestamp = timestamp + 1'b1;
   else
      timestamp = 0;
   
   out_align_err = 1'b0;   
   spill_on = 1'b0;
   
   waddr_et   = waddr_et   + 1'b1;
   waddr_veto = waddr_veto + 1'b1;
    
   if( align_cnt < MAX_ALIGN_TIME )
      align_cnt = align_cnt + 1'b1;
   else
      out_align_err = 1'b1;
     
   out_adc = 1'b0;
   out_rst = 1'b0;
 
   et_tmp2 = et_tmp1;
   et_tmp1 = et_tmp0;
   et_tmp0 = 17'b0_0000_0000_0000_0000;
   
   veto_tmp2 = veto_tmp1;
   veto_tmp1 = veto_tmp0;
   veto_tmp0 = 16'b0000_0000_0000_0000;
 
// issue align trigger at the moment that live is on 
   if( in_live == 1 && pre_live == 0 )
      begin
         timestamp = 0;
         spill_on = 1'b0;
         waddr_et = 1;
         waddr_veto = 1;
         raddr_et = 0;
         raddr_veto = 0;
         et_tmp0 = 0;
         et_tmp1 = 0;
         et_tmp2 = 0;
         veto_tmp0 = 0;
         veto_tmp1 = 0;
         veto_tmp2 = 0;
         align_cnt = 9'b1_1111_1111;
         rena = 0;
         align_ena = 1'b0;
         out_delay_et = 0;
      end

// enable trigger decision if live is on and input error = 0
   if( in_live == 1'b1 && in_err == 1'b0 )
      out_ena = 1'b1;
   else
      out_ena = 1'b0;
    
  out_adc = 1;
  
   if( in_err == 1'b1 )
      out_rst = 1'b1;
  
// align trigger
   if(    timestamp >= DELAY_ALIGN && in_err == 0 
       && align_ena == 1'b0  
     )
      begin
         align_ena = 1'b1;
         align_cnt = 0;
         out_adc = 1'b1;
      end
   else if( align_cnt == 2 )
      out_adc = 1'b1;   
   
// read et / veto from memory
   et_tmp0 = in_et;
   if(    et_tmp0 == 17'b0_0000_0000_0000_0000
       && et_tmp1 == 17'b1_1111_1110_1111_1110
       && et_tmp2 == 17'b0_0000_0000_0000_0000 )
      begin
          raddr_start_et = waddr_et - 2;
          out_delay_et = align_cnt;
      end
   
   veto_tmp0 = in_veto;
   if(    veto_tmp0 == 16'b0000_0000_0000_0000
       && veto_tmp1 == 16'b1111_1110_1111_1110
       && veto_tmp2 == 16'b0000_0000_0000_0000 )
      begin
         raddr_start_veto = waddr_veto - 2;
      end  
   
//    
   if( align_cnt == raw_delay )
      begin
         rena = 1'b1;
         raddr_et = raddr_start_et;
         raddr_veto = raddr_start_veto;
         
         spill_on = 1'b1;
      end

   if( rena==1'b1 )
      begin
         out_is_et = q_wire_et[16];
         out_raw_et = q_wire_et[15:0];
         out_veto = q_wire_veto;
         raddr_et = raddr_et + 1'b1;
         raddr_veto = raddr_veto + 1'b1;
      end

//// KEEP LAST ////
   out_spill = spill_on;  
   pre_live = in_live;
   
   if( timestamp > SPILL_LENGTH )
      spill_on = 1'b0;
      
end

endmodule