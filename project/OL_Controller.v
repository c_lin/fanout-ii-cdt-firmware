module OL_Controller(	
	clk,
	LIVE,
	data_tx,
	data_rx,
	ena_rx,
	data_out,
	ena_tx,
	datak,
	error,
);

input wire clk;
input wire LIVE;
input wire [15:0] data_tx;
input wire [15:0] data_rx;
input wire ena_rx;

output reg [15:0] data_out;
output reg ena_tx = 1'b1;
output reg [1:0] datak;
output reg error;

reg [15:0] pattern_align = 16'h50BC;
reg [15:0] pattern_check = 16'hFFEE;
reg [1:0] mode = 2'b11; // 0: alignment mode 1: test mode, 2: data mode

reg [19:0] control = 20'b0;
reg [10:0] cnt_pattern = 11'b0;

always @(posedge clk)
begin
	
	mode = (LIVE==1'b0)? 2'b00:mode;
	
	if(mode==2'b00) // alignment
	begin
	
		data_out = pattern_align; 
		ena_tx = (control<20'hFDDDD)? 1'b0 : 1'b1;
		datak = (control<20'hFDDDD)? 2'b11 : 2'b00;	
		mode = (control==20'hFEEEE && LIVE==1'b1)? 2'b01 : mode;	
		error = 1'b1;
		cnt_pattern = 11'b0;
		
		control = control + 1'b1;
		
	end
	else if(mode==2'b01) // test mode
	begin
	
		ena_tx = 1'b1;
		datak = 2'b00;
		data_out = pattern_check;
		mode = (control==20'hFFFFF)? 2'b10 : mode;
		cnt_pattern = (data_rx==pattern_check)? cnt_pattern + 1'b1 : 10'b0;
		error = (cnt_pattern==11'b11111111111)? 1'b0 : error;
		
		control = control + 1'b1;
	end
	else if(mode==2'b10) // data mode
	begin
	
		ena_tx = 1'b1;
		datak = 2'b00;
		data_out = data_tx;
		
	end
	
	error = (ena_rx==1'b0)? 1'b0 : error;
	
end

endmodule
