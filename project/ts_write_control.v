/* 
   spill_counter
   C. Lin, chiehlin@uchicago.edu
	
	2021.12.23
	Preliminary version.
*/

module ts_write_control
(
// input 
  clk               , // system clock
  
  // inputs
  in_L1A            ,
  clear             ,
  live              ,
  
  // output
  out_wren          ,
  out_waddr         
);


input wire         clk;

// inputs
input wire         in_L1A;
input wire         clear;
input wire         live;

// output
output reg         out_wren;
output reg [15 :0] out_waddr;

reg lock;
reg reset;

////////////////////////////////////////////
always @(posedge clk) begin

   if( lock==1'b0 && in_L1A==1'b1 ) begin
      out_wren <= 1'b1;
      out_waddr <= out_waddr + 1;
   end

   if( live == 1'b0 ) begin
	   lock <= 1'b1; 
   end
   
   if( reset==1'b1 && live==1'b1 ) begin
      lock <= 1'b0;
      out_waddr <= 16'hFFFF;
      reset <= 1'b0;
   end
   
   if( clear==1'b1 ) begin
      reset <= 1'b1;
   end
	
end

endmodule