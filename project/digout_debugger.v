/* 
   Local CDT Design
  
        
*/

module digout_debugger
(
// input 
   clk               , // system clock
   rst               ,
   user_ena          ,
     
// output
   out               ,
             
);

input  wire         clk;
input  wire         rst;
input  wire         user_ena;

output reg          out;

reg         [15 :0] cycle_cnt;
reg         [5  :0] ibit;
reg         [35 :0] data;

always @(posedge clk)
begin

   if( rst == 1'b1 )
      begin
         out = 1'b0;
         cycle_cnt = 0;
         ibit = 0;
      end

   if( ibit == 35 )
      cycle_cnt = cycle_cnt + 1;

   data = { 2'b11 , cycle_cnt , 18'b00_0000_0000_0000_0000 };
   out = data[ibit];
    
   if( ibit == 0 )
      ibit = 35;
   else 
      ibit = ibit - 1; 
      
   if( user_ena == 1'b0 )
      out = 1'b0;
          
end

endmodule