/* 
   Fanout CDT Design
   C. Lin, 2018.04.11
*/

module tlk_err_assembler
#( parameter LENGTH_ERR = 21  // (16+4) +1 (excluding 3 header)
  )
///
(
// input 
   clk               , // system clock
   in_live           ,
   in_bus0           ,
   in_bus1           ,
   in_bus2           ,
   in_bus3           ,
   in_bus4           ,
   in_bus5           ,
   in_bus6           ,
   in_bus7           ,
   in_bus8           ,
   in_bus9           ,
   in_bus10          ,
   ena               ,
   in_this_err       ,
     
// output
   out_lvds          ,
             
);

input  wire         clk;
input  wire         in_live;
input  wire [20 :0] in_bus0;
input  wire [20 :0] in_bus1;
input  wire [20 :0] in_bus2;
input  wire [20 :0] in_bus3;
input  wire [20 :0] in_bus4;
input  wire [20 :0] in_bus5;
input  wire [20 :0] in_bus6;
input  wire [20 :0] in_bus7;
input  wire [20 :0] in_bus8;
input  wire [20 :0] in_bus9;
input  wire [20 :0] in_bus10;
input  wire         ena;
input  wire         in_this_err;

output reg          out_lvds;

reg         [1 :0] iheader;
reg         [3 :0] ibus;
reg         [4 :0] ibit;

always @(posedge clk)
begin

   out_lvds = 1'b0;

   if( in_live == 1'b0 )
      begin   
         iheader = 0;
         ibus = 0;
         ibit = 0; 
      end
   
   if( ena == 1'b0 )
      begin
         iheader = 0;
         ibus = 0;
         ibit = 0;
      end
   
         ////////////////////
         /// header words ///
         ////////////////////
      
         if( iheader == 0 && ena == 1'b1 )
            out_lvds = 1'b1;
      
         if( iheader == 1 )
            out_lvds = 1'b0;
         
         if( iheader == 2 )
            out_lvds = 1'b0;
      
         ////////////////////
         // once iheader == 3, means headers were fully sent out,
         // start to read the data from "err_cont"
         ///

         if( iheader == 3 && ibus < 11 )
            begin
               case( ibus )
                  0 :  out_lvds = in_bus0[ibit];
                  1 :  out_lvds = in_bus1[ibit];
                  2 :  out_lvds = in_bus2[ibit];
                  3 :  out_lvds = in_bus3[ibit];
                  4 :  out_lvds = in_bus4[ibit];
                  5 :  out_lvds = in_bus5[ibit];
                  6 :  out_lvds = in_bus6[ibit];
                  7 :  out_lvds = in_bus7[ibit];
                  8 :  out_lvds = in_bus8[ibit];
                  9 :  out_lvds = in_bus9[ibit];
                  10:  out_lvds = in_bus10[ibit];
               endcase
               
               if( ibit < LENGTH_ERR )
                  ibit = ibit + 1;
               
               if( ibit == LENGTH_ERR )
                  begin
                     ibit = 0;
                     ibus = ibus + 1;
                  end   
            
            end
        
         //// last bit ////    
            
         else if( iheader == 3 && ibus == 11 )
            begin
               out_lvds = in_this_err;
               ibus = ibus + 1;
            end
         
         
          /// keep in the last part ///
          /// the counter only starts if enabled ///
          if( ena == 1'b1 && iheader < 3 )
             iheader = iheader + 1;
         
end

endmodule