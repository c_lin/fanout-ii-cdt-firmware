/* 
   Top CDT Design
   lv1a_pipeline block
   C. Lin, 2018.03.22
   
*/

module lv1a_pipeline
// const parameter
#( parameter PIPE = 512 ,
   parameter CONT = 32  ,
   parameter GATE = 20  
  )
// arguments
(
// input 
  clk               , // system clock
  
  in_rst            , // reset counter
  in_lv1a_type0     , // lv1a input from type block (1  bit )
  in_lv1a_type1     , // lv1a input from type block (1  bit )
  in_lv1a_type2     , // lv1a input from type block (1  bit )
  in_lv1a_type3     , // lv1a input from type block (1  bit )
  in_lv1a_type4     , // lv1a input from type block (1  bit )
  in_lv1a_type5     , // lv1a input from type block (1  bit )
  in_lv1a_type6     , // lv1a input from type block (1  bit )
  in_lv1a_type7     , // lv1a input from type block (1  bit )
  in_nclus          , // input cluster              (16 bits)
  
  in_lv1_inhibit    , // expecting time to issue lv1
  
  delay_plv1        , // delay of plv1
  delay_out_lv1a    , // delay of lv1a to lv1b type block
  
// output
  out_lv1a_type0    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type1    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type2    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type3    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type4    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type5    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type6    , // lv1a output to lv1b type block (1 bit)
  out_lv1a_type7    , // lv1a output to lv1b type block (1 bit)
  out_nclus         , // 4 bits
  out_plv1          , // output to trigger line

  lv1a_raw_cnt      ,
  lv1a_cnt          

);

// input
input wire           clk;

input wire           in_rst;
input wire           in_lv1a_type0;
input wire           in_lv1a_type1;
input wire           in_lv1a_type2;
input wire           in_lv1a_type3;
input wire           in_lv1a_type4;
input wire           in_lv1a_type5;
input wire           in_lv1a_type6;
input wire           in_lv1a_type7;
input wire  [15  :0] in_nclus; 
input wire           in_lv1_inhibit;

input wire  [8   :0] delay_plv1;
input wire  [8   :0] delay_out_lv1a;

// output
output reg           out_lv1a_type0;
output reg           out_lv1a_type1;
output reg           out_lv1a_type2;
output reg           out_lv1a_type3;
output reg           out_lv1a_type4;
output reg           out_lv1a_type5;
output reg           out_lv1a_type6;
output reg           out_lv1a_type7;
output reg  [3   :0] out_nclus;
output reg           out_plv1;

output reg  [24  :0] lv1a_raw_cnt;
output reg  [24  :0] lv1a_cnt;

// variables 
reg        [CONT-1:0] cont_lv1a_type0;
reg        [CONT-1:0] cont_lv1a_type1;
reg        [CONT-1:0] cont_lv1a_type2;
reg        [CONT-1:0] cont_lv1a_type3;
reg        [CONT-1:0] cont_lv1a_type4;
reg        [CONT-1:0] cont_lv1a_type5;
reg        [CONT-1:0] cont_lv1a_type6;
reg        [CONT-1:0] cont_lv1a_type7;
reg        [PIPE-1:0] pipeline_lv1a;

reg        [4   :0] in_lv1a_id;
reg        [4   :0] out_lv1a_id;

// for nclus_mem
reg        [9   :0] raddr;
reg        [9   :0] waddr;
reg        [3	:0] ncluster;

reg wena = 1'b0;
reg rena = 1'b0;

/// function include
wire [3:0] q_wire;
nclus_mem _nclusmem(clk,ncluster,raddr,waddr,wena,q_wire);

////////////////////////////////////////////
always @(posedge clk)
begin
 
  if( in_rst == 1'b1 )
     begin
		wena = 1'b0;
        in_lv1a_id = 5'b0;
        out_lv1a_id = 5'b0;
        lv1a_raw_cnt = 24'b0;
        lv1a_cnt     = 24'b0;
          
        cont_lv1a_type0  = 512'b0;
        cont_lv1a_type1  = 512'b0;
        cont_lv1a_type2  = 512'b0;
        cont_lv1a_type3  = 512'b0;
        cont_lv1a_type4  = 512'b0;
        cont_lv1a_type5  = 512'b0;
        cont_lv1a_type6  = 512'b0;
        cont_lv1a_type7  = 512'b0;
        pipeline_lv1a    = 512'b0;
        
     end

// read nclus from OFC-II for each clock
//// input structure: 
//////// [9:0] id, 
//////// [13:10] nclus, 
//////// [15] 0: error 1: (id + nclus) is sent 
   if( in_nclus[15] == 1'b1 )
      begin
		 wena = 1'b1;
         waddr = in_nclus[9:0];
         ncluster = in_nclus[13:10];
      end 
   else
		 wena = 1'b0;

// pipeline
   pipeline_lv1a = pipeline_lv1a << 1;
   pipeline_lv1a[0] = 1'b0;
 
  if(    in_lv1a_type0 == 1 || in_lv1a_type1 == 1
      || in_lv1a_type2 == 1 || in_lv1a_type3 == 1 
      || in_lv1a_type4 == 1 || in_lv1a_type5 == 1 
      || in_lv1a_type6 == 1 || in_lv1a_type7 == 1 
    )
      begin
         lv1a_raw_cnt = lv1a_raw_cnt + 1;
         if( pipeline_lv1a[GATE-1:0] == 0 && in_lv1_inhibit == 1'b0 )
            begin
               pipeline_lv1a[0] = 1'b1;
               cont_lv1a_type0[in_lv1a_id] = in_lv1a_type0;
               cont_lv1a_type1[in_lv1a_id] = in_lv1a_type1;
               cont_lv1a_type2[in_lv1a_id] = in_lv1a_type2;
               cont_lv1a_type3[in_lv1a_id] = in_lv1a_type3;
               cont_lv1a_type4[in_lv1a_id] = in_lv1a_type4;
               cont_lv1a_type5[in_lv1a_id] = in_lv1a_type5;
               cont_lv1a_type6[in_lv1a_id] = in_lv1a_type6;
               cont_lv1a_type7[in_lv1a_id] = in_lv1a_type7;
               
               in_lv1a_id = in_lv1a_id + 1;
               
            end
      end  

// pre-lv1 issuing
   out_plv1 = 0;
   if( pipeline_lv1a[ delay_plv1 +: 3 ] > 0 )
      out_plv1 = 1;
     
// send lv1a to lv1b type block
   out_nclus = q_wire;   
   //// p.s.: 2 clock lantency from memory readout 
   if( pipeline_lv1a[delay_out_lv1a-3])
      begin
         raddr = lv1a_cnt[9:0];
         out_nclus = q_wire;
      end
    
   if( pipeline_lv1a[delay_out_lv1a] )
      begin
         out_lv1a_type0 = cont_lv1a_type0[out_lv1a_id];
         out_lv1a_type1 = cont_lv1a_type1[out_lv1a_id];
         out_lv1a_type2 = cont_lv1a_type2[out_lv1a_id];
         out_lv1a_type3 = cont_lv1a_type3[out_lv1a_id];
         out_lv1a_type4 = cont_lv1a_type4[out_lv1a_id];
         out_lv1a_type5 = cont_lv1a_type5[out_lv1a_id];
         out_lv1a_type6 = cont_lv1a_type6[out_lv1a_id];
         out_lv1a_type7 = cont_lv1a_type7[out_lv1a_id];
         
         /// ncluster ///
         //raddr = (lv1a_cnt % 1024 );
         //raddr = lv1a_cnt[9:0] + 1'b1;
         
//         out_nclus = q_wire;    
         
         out_lv1a_id = out_lv1a_id + 1;
                                  
         lv1a_cnt = lv1a_cnt + 1;
            
      end  
   else
      begin
         out_lv1a_type0 = 1'b0;
         out_lv1a_type1 = 1'b0;
         out_lv1a_type2 = 1'b0;
         out_lv1a_type3 = 1'b0;
         out_lv1a_type4 = 1'b0;
         out_lv1a_type5 = 1'b0;
         out_lv1a_type6 = 1'b0;
         out_lv1a_type7 = 1'b0;
         
//         out_nclus = 4'b0;
      end 
      
end

endmodule
