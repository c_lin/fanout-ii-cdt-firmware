/* 
   Fanout CDT Design
   C. Lin, 2018.04.11 
*/

/*
   Result of TLK error should be sent after switching to data mode in ADC,
   It takes 0.08 sec at most.
   
   If there is no result obtained in fanout CDT at 0.095 sec (11875000 clks)
   , force sending error bits with last bit on.
   
   - v0.10 (2018.04.16)
     * Let the max_time become a control register instead 
    
*/

module tlk_err_controller
(
// input 
   clk               , // system clock
   in_live           ,
   user_masking      ,
   bus_condition     ,
   
   max_time          ,
  
// output
   out_ena           ,
   out_this_err      
             
);

input  wire         clk;
input  wire         in_live;
input  wire [10 :0] user_masking;
input  wire [10 :0] bus_condition;

input  wire [31 :0] max_time;

output reg          out_ena;
output reg          out_this_err;

reg                 got_all;
reg         [10 :0] bus_pattern;

reg         [31 :0] timestamp;

always @(posedge clk)
begin

   if( in_live == 1'b0 )
      begin
         timestamp = 0;
         out_ena = 1'b0;
         out_this_err = 1'b0;
         got_all = 1'b0;
      end
   else 
      timestamp = timestamp + 1;

   out_ena = 1'b0;

   if( in_live )
      begin
		 bus_pattern = 11'b111_1111_1111;
		 bus_pattern = bus_pattern ^ user_masking;
   
		 if( (bus_condition & bus_pattern ) == bus_pattern )
			 got_all = 1'b1;

		 if( timestamp == max_time && got_all == 1'b0 )
		    begin
			   out_this_err = 1'b1;
			   got_all = 1'b1;
		    end
      
		 if( got_all == 1'b1 )
			 out_ena = 1'b1;
		 else
			 out_ena = 1'b0;
   end
   
end

endmodule