/* 
   busy_counter
   C. Lin, chiehlin@uchicago.edu
   
   This module is designed to measure how many times busy rises in a spill.
*/

module busy_counter
(
// input 
  clk               , // system clock
  
  // inputs
  reset             ,
  busy              ,
  
  // output
  q
);


input wire         clk;

// inputs
input wire         reset;
input wire         busy;

// output
output reg [15 :0] q;

//
reg pre_busy;

////////////////////////////////////////////
always @(posedge clk) begin

	if( reset == 1'b1 ) begin
	   q = 0;
	   pre_busy = 0;
	end

    if( pre_busy==1'b0 && busy==1'b1 ) begin
       q = q + 1;
    end
   
    pre_busy = busy;
	
end

endmodule
