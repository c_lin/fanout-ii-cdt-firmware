/* 
   Fanout CDT Design
   C. Lin, 2018.04.11
*/

module output_switch_controller
(
// input 
   clk               , // system clock
   ena_tlk           ,
   ena_dc            ,
     
// output
   out_switch_val    
        
   
);

input  wire         clk;
input  wire         ena_tlk;
input  wire         ena_dc;

output reg  [1  :0] out_switch_val;

always @(posedge clk)
begin

   out_switch_val = ena_tlk + ena_dc;
  
end

endmodule