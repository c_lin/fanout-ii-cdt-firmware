/* 
   
   
*/

module et_err_sim_trig
#( parameter NEVENTS = 2048
  )
//
(
// input 
  clk               , // system clock
  
  // inputs after alignment and judgement of et
  in_live           , // reset
  user_ena          , 
  trig_to_adc       ,
  
  out_rena          ,
  out_raddr         
);

input wire         clk;

// inputs
input wire         in_live;
input wire         user_ena;
input wire         trig_to_adc;

// output
output reg         out_rena;
output reg [11 :0] out_raddr;

//
reg                is_tlk_err_issued;
reg                is_dc_err_issued;
reg        [11 :0] cnt;

reg        [3  :0] pipeline;

reg                got_align_trig;

always @(posedge clk)
begin
 
   if( in_live == 1'b0 )
      begin
         out_rena = 1'b0;
         cnt = 0;
         is_tlk_err_issued = 1'b0;
         is_dc_err_issued = 1'b0;
         pipeline = 0;
         got_align_trig = 1'b0;
      end
   
   if( user_ena == 1'b1 && in_live == 1'b1 )
      begin
      
         //////
         /// judge the previous pipeline is align trigger or not
         /// (only care about align trig, and only once per spill)
         if( pipeline == 4'b1010 && got_align_trig == 1'b0 )
            got_align_trig = 1'b1;
            
         //////
         // before getting align trig, keep the pipeline movement
         
         if( got_align_trig == 1'b0 )
            begin
               pipeline = pipeline << 1;
               pipeline[0] = trig_to_adc;
            end
            
         /// If dc error did not issue yet and the alignment trigger was obtained,
         /// start the counter
         if( is_dc_err_issued == 1'b0 && got_align_trig == 1'b1 )
            begin
               if( cnt < NEVENTS + NEVENTS )
                  begin
                     out_rena = 1'b1;
                     out_raddr = cnt;
                     cnt = cnt + 1;
                  end
                  
               /// the last raddr is 4095
               /// after cnt = cnt + 1, it goes back to 0
               //////
               if( cnt == 0 )
                  begin
                     out_rena = 1'b0;
                     is_dc_err_issued = 1'b1;
                  end
            end
            
         /// first 2048 samples starts once live is on ///
         if( cnt < NEVENTS && is_tlk_err_issued == 1'b0 )
            begin
               out_rena = 1'b1;
               out_raddr = cnt;
               cnt = cnt + 1;              
            end
         
         ////////
         /// out_raddr = 2047 but cnt == 2048 now
         /// read disable
         if( cnt == NEVENTS && is_tlk_err_issued == 1'b0 )
            begin
               is_tlk_err_issued = 1'b1;
               out_rena = 1'b0;
            end
           
 
       
        ///////
         
      end
      
end

endmodule