/* 
   Top CDT Design
   trig_type_lv1a block
   C. Lin, 2018.03.27, ver 3
   
   * log
   v1: add switch "AND"."OR"
   v2: remove prescale
*/

module trig_type_lv1a
(
// input 
  clk               , // system clock
  
  // inputs
  in_et             , // et input     ( 1  bit  )
  in_veto           , // veto input   ( 16 bits )
  in_rst            , // reset signal ( 1  bit  )
  in_ena            , // enable       ( 1  bit  )
  in_spill          , // spill        ( 1  bit  ) 
  in_ext0           ,
  in_ext1           ,
  in_ext2           ,
  in_ext3           ,
  
  // register
  user_et           ,
  user_veto         ,
  user_veto_as_trig , 
  user_and_or       ,
  user_ena          ,
  user_spill_on     ,
  user_spill_off    ,
  user_ext          ,
 
  // output
  out_lv1a                        
);

input wire         clk;

// inputs
input wire         in_et;
input wire [15 :0] in_veto;
input wire         in_rst;
input wire         in_ena;
input wire         in_spill;
input wire         in_ext0;
input wire         in_ext1;
input wire         in_ext2;
input wire         in_ext3;

// reg
input wire         user_et;
input wire [15 :0] user_veto;
input wire [15 :0] user_veto_as_trig;
input wire         user_and_or;
input wire         user_ena;
input wire         user_spill_on;  // collect events during spill on?
input wire         user_spill_off; // collect events during spill off?
input wire [3  :0] user_ext;

// output
output reg         out_lv1a;

// variables 
reg        [15 :0] raw_cnt;
reg                is_trig;

////////////////////////////////////////////
always @(posedge clk)
begin
 
// reset counter if requested
   if(in_rst)
      begin
         raw_cnt = 0;
      end

// check whether it satisfies any trigger type
   is_trig = 0;
   if(     user_ena == 1 && in_ena == 1
       && (    ( user_spill_on  == 1 && in_spill==1 )
            || ( user_spill_off == 1 && in_spill==0 ) 
          )
     )
      begin
   
      if( user_ext > 0 ) // external trigger
         begin
            if(    ( user_ext[0]==1 && in_ext0==1 )
                || ( user_ext[1]==1 && in_ext1==1 )
                || ( user_ext[2]==1 && in_ext2==1 )
                || ( user_ext[3]==1 && in_ext3==1 ) 
              )
               is_trig = 1;
         end
   
      else if( user_and_or == 1 ) // internal AND
         begin
            if(    (user_et & in_et) == user_et 
                && (user_veto_as_trig & in_veto)== user_veto_as_trig
                && (user_veto & in_veto) == 16'b0
              )
               is_trig = 1;
         end
      else                       // internal OR
        begin
            if( user_et == 1 )
               begin
                  if(    ( in_et==1 || (user_veto_as_trig & in_veto) > 16'b0 )
                      && (user_veto & in_veto) == 16'b0
                    )
                     is_trig = 1;
               end
            else
               begin
                  if(    (user_veto_as_trig & in_veto) > 16'b0 
                      && (user_veto & in_veto) == 16'b0
                    )
                     is_trig = 1;
               end
         end  
   end
       
// trigger issuing
   out_lv1a = 0;
   if( is_trig == 1 )
      begin
         raw_cnt = raw_cnt + 1;
         out_lv1a = 1;   
      end
      
end

endmodule