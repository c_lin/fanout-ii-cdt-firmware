# FANOUT-II CDT firmware

FANOUT-II CDT serves clock / trigger / LIVE to OFC-I modules and report errors to TOP CDT. 

## Version

2022 Mar 25, v0.02.01
Recommended Quartus version: v9.0 (64 bit)

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/c_lin/fanout-ii-cdt-firmware.git
git branch -M main
git push -uf origin main
```

## Contact

Chieh Lin (chiehlin@uchicago.edu)
